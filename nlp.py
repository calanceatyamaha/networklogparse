import glob, os, csv, sys
from xlsxwriter.workbook import Workbook
from Tkinter import *
from tkFileDialog import *
# from gui import *

class Application(Frame):

    def __init__(self, master):
        Frame.__init__(self,master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.button1 = Button(self, text = "Open Cisco Logs")
        self.button1["command"] = self.openfile
        self.button1.grid()

        self.button2 = Button(self, text = "Save .xlsx")
        self.button2["command"] =self.saveas
        self.button2.grid()

        self.button3 = Button(self, text = "Concate")
        self.button3["command"] = self.complie_split()
        self.button3.grid()

        self.button5 = Button(self, text = "Make xlsx")
        self.button5["command"] = self.csv_xlsx()
        self.button5.grid()

        self.button4 = Button(self, text = "Quit")
        self.button4["command"] = root.quit
        self.button4.grid()

    def saveas(self):
        savef = asksaveasfilename(defaultextension='.xlsx')
        return savef

    def openfile(self):
        openf = askdirectory()
        return openf

    def complie_split(self):
        path = self.openfile()
        p = os.path.dirname(self.saveas())
        for f in glob.glob(os.path.join(path, '*.txt')):

            types = ['.Alert', '.Critical', '.Error', '.Warning' ]
            for t in types:

                with open( f, "r" ) as ins:
                    array = []
                    for line in ins:
                        if t in line:
                            array.append(line)
                thefile = open( p + "/log" + t + ".csv", "w+" )
                for i in array:
                    thefile.write("%s" % i)

    def csv_xlsx(self):
        p = self.saveas()
        workbook = Workbook(p + '/logs.xlsx')
        log_path = os.listdir(p)
        for csvfile in log_path:
            if ".csv" in csvfile:
                # print csvfile
                worksheet = workbook.add_worksheet(csvfile) #wroskeet with csv file name
                with open(p + "/" + csvfile, 'rb') as f:
                    reader = csv.reader(f)
                    for r, row in enumerate(reader):
                        for c, col in enumerate(row):
                            worksheet.write(r, c, col) #write the csv file content into it
        workbook.close()

    # def run_app():
    #     complie_split()
    #     csv_xlsx()

if __name__ == '__main__':
    root = Tk()
    root.title("Cisco Log to Excel")
    root.minsize(width=200, height=200)
    root.maxsize(width=200, height=200)
    app = Application(root)

    menubar = Menu(root)
    filemenu = Menu(menubar)
    filemenu.add_separator()
    filemenu.add_command(label="Quit", command=root.quit)
    menubar.add_cascade(label="File", menu=filemenu)

    root.config(menu=menubar)
    root.mainloop()
