import glob, os, csv, sys
from xlsxwriter.workbook import Workbook
from gui import *

def get_log_dir():
    print "Choose a the directory where the logs are located:"
    path = raw_input()
    return path

def get_output_dir():
    print "Where would you like the logs to be saved?"
    p = raw_input()
    return p

def complie_split():
    path = openfile()
    p = saveas()
    for f in glob.glob(os.path.join(path, '*.txt')):

        types = ['.Alert', '.Critical', '.Error', '.Warning' ]
        for t in types:

            with open( f, "r" ) as ins:
                array = []
                for line in ins:
                    if t in line:
                        array.append(line)
            thefile = open( p + "/log" + t + ".csv", "w+" )
            for i in array:
                thefile.write("%s" % i)

def csv_xlsx():
    p = saveas()
    workbook = Workbook(p + '/logs.xlsx')
    log_path = os.listdir(p)
    for csvfile in log_path:
        if ".csv" in csvfile:
            # print csvfile
            worksheet = workbook.add_worksheet(csvfile) #wroskeet with csv file name
            with open(p + "/" + csvfile, 'rb') as f:
                reader = csv.reader(f)
                for r, row in enumerate(reader):
                    for c, col in enumerate(row):
                        worksheet.write(r, c, col) #write the csv file content into it
    workbook.close()

def run_app():
    complie_split()
    csv_xlsx()

# if __name__ == '__main__':
#     complie_split()
#     csv_xlsx()
