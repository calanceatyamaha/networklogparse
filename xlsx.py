import glob, os, csv
from xlsxwriter.workbook import Workbook

def txt_xlsx():
    workbook = Workbook('logs.xlsx')
    path = os.listdir('sorted_logs/')
    for csvfile in path:
        if ".csv" in csvfile:
            # print csvfile
            worksheet = workbook.add_worksheet(csvfile) #wroskeet with csv file name
            with open('sorted_logs/' + csvfile, 'rb') as f:
                reader = csv.reader(f)
                for r, row in enumerate(reader):
                    for c, col in enumerate(row):
                        worksheet.write(r, c, col) #write the csv file content into it
    workbook.close()

if __name__ == '__main__':
    txt_xlsx()
