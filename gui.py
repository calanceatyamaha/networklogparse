from Tkinter import *
from tkFileDialog import *
from app import *

class Application(Frame):

    def __init__(self, master):
        Frame.__init__(self,master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.button1 = Button(self, text = "Open Cisco Logs")
        self.button1["command"] = self.openfile
        self.button1.grid()

        self.button2 = Button(self, text = "Save .xlsx")
        self.button2["command"] =self.saveas
        self.button2.grid()

        self.button3 = Button(self, text = "Run")
        self.button3["command"] = run_app()
        self.button3.grid()

        self.button4 = Button(self, text = "Quit")
        self.button4["command"] = root.quit
        self.button4.grid()

    def saveas(self):
        savef = asksaveasfilename(defaultextension='.xlsx')
        return savef

    def openfile(self):
        openf = askdirectory()
        return openf



root = Tk()
root.title("Cisco Log to Excel")
root.minsize(width=200, height=200)
root.maxsize(width=200, height=200)
app = Application(root)

menubar = Menu(root)
filemenu = Menu(menubar)
# filemenu.add_command(label="Open", command=Application.openfile)
# filemenu.add_command(label="Save As", command=Application.saveas)
filemenu.add_separator()
filemenu.add_command(label="Quit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

root.config(menu=menubar)
root.mainloop()
