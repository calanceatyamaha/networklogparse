teir1 = {
    "Isuzu Support Center (Dealer)": "7733828353",
    "Isuzu Internal": "8444936453",
    "Yamaha User Support": "8444472607",
    "Nobel Biocare": "8445529211",
    "Kubota Internal": "8444544261",
    "Kubota Dealer": "8445062362"
}

teir2 = {
    "Suzuki Help Desk": "8445434043",
    "Mitsubishi Hotline": "7088984526",
    "Westview": "8444450413"
}

teir3 = {
    "Calance Dispatch": "9095818051",
    "Pacific Hydrotech": "8445448336",
    "Dimension Integration": "8445526255"
}
