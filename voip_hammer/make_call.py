from twilio.rest import Client

def make_call(tel):
    # Your Account Sid and Auth Token from twilio.com/console
    ### Test Credentials
    account_sid = 'ACcfa6466028dc7395c4a21077bded9a06'
    auth_token = 'ad78d05056567f4c3e2f8edc940e81bb'
    ###Live Credentials
    # account_sid = 'AC926271028952efc3c07a5ba32d094437'
    # auth_token = 'c523b7d31bd5dfd9f6a361f2d2196acb'
    client = Client(account_sid, auth_token)

    call = client.calls.create(
                            to='+1' + str(tel),
                            ###Test From Number
                            from_='+15005550006',
                            ###Live From Number
                            # from_='+19514634903',
                            url='https://handler.twilio.com/twiml/EH7721db92ffcfa2a0818aa7e4b4607f9a'
                        )

    return(call.sid)

# print(make_call(6263271014))
